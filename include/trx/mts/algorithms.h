/*
Copyright (c) 2016
*/

#pragma once

#include <mutex>
#include <thread>
#include <trx/mts/stream.h>
#include <sys/sem.h>

namespace trx {

	/**
	   Parallelization Template class
	   ------------------------------

	   Template class that will hold the actual parallelization algorithm. Exposes interaction methods
	   with the algorithm classes and allows semaphore based waiting for worker threads.
	 **/
	template <class C>
	class ParallelData {
	private:
		std::shared_ptr< C > __target;
		int __sem;

	public:
		inline ParallelData() : __target(new C()) {
			key_t _key = ftok("/usr/bin/trx-slave", 1);
			this->__sem = semget(_key, 1, IPC_CREAT | 0777);
		}
		inline virtual ~ParallelData() {
			semctl(this->__sem, 0, IPC_RMID);
		}

		// Waits for some transaction to be available (waits on semaphore)
		// and invoces the target class 'pop' method, returning the available
		// transation
		inline virtual auto pop() -> decltype(this->__target->pop()) {
			this->wait();
			return this->__target->pop();
		}

		// Signals that the instruction associated with the given transaction
		// has finished
		inline virtual auto commit(trx::transaction _trx) -> void {
			this->__target->finish(_trx);
		}

		// Inserts the given transaction into the queue, according  to the
		// target parallelization algorithm
		inline virtual auto push(trx::transaction _trx) -> void {
			this->__target->push(_trx);
		}

		// Adds 1 to the synchronization semaphore
		inline virtual auto notify() -> void {
			struct sembuf _unlock[1] = { { (short unsigned int) 0, 1 } };
			semop(this->__sem, _unlock, 1);
		}

		// Substracts 1 from the synchronization semaphore
		inline virtual auto wait() -> void {
			struct sembuf _wait[1] = { { (short unsigned int) 0, -1 } };
			semop(this->__sem, _wait, 1);
		}
		
	};
	/**
	   Smart pointer class to encapsulate the parallelization template 
	 **/
	template <class C>
	class parallel : public std::shared_ptr< trx::ParallelData< C > > {
	public:
		inline parallel() : std::shared_ptr< trx::ParallelData< C > >(new trx::ParallelData< C >()) {
		}
		inline virtual ~parallel() {}

	};
	
	namespace algorithm {

		/**
		   Parallelization Abstract class
		   ------------------------------

		   Abstract class template for parallelization algorithms. Exposes interaction methods
		   with the algorithm classes and allows semaphore based waiting for worker threads.
		**/
		class Parallelization {
		public:
			inline Parallelization() {
				key_t _key = ftok("/usr/bin/trx-slave", 1);
				this->__sem = semget(_key, 1, IPC_CREAT | 0777);
			}
			virtual ~Parallelization() {
				semctl(this->__sem, 0, IPC_RMID);
			}

			// Retrieves the next transaction to be processed and removes
			// it from the queue, according to the used algorithm
			virtual auto pop() -> trx::transaction = 0;
			// Inserts the given transaction into the waiting queue
			virtual auto push(trx::transaction _trx) -> void = 0;
			// Signals that the given transaction has finished and rearranges
			// the queue
			virtual auto finish(trx::transaction _trx) -> void = 0;

			// Adds 1 to the synchronization semaphore
			inline virtual auto notify() -> void {
				struct sembuf _unlock[1] = { { (short unsigned int) 0, 1 } };
				semop(this->__sem, _unlock, 1);
			}

			// Substracts 1 from the synchronization semaphore
			inline virtual auto wait() -> void {
				struct sembuf _wait[1] = { { (short unsigned int) 0, -1 } };
				semop(this->__sem, _wait, 1);
			}
			
		private:
			int __sem;
		};
		
		/**
		   Table Parallelization class
		   ------------------------------

		   So in this algorithm, the coordinator will look at the table of the transaction.

		   Requirement 1: Requests from different tables can be executed in parallel
		   Requirement 2: Requests for the same table execute sequentially. 
		**/
		class TableParallelization : public trx::algorithm::Parallelization {
		public:
			TableParallelization();
			virtual ~TableParallelization();

			// Retrieves the next transaction to be processed and removes
			// it from the queue
			virtual auto pop() -> trx::transaction;
			// Inserts the given transaction into the waiting queue
			virtual auto push(trx::transaction _trx) -> void;
			// Signals that the given transaction has finished and rearranges
			// the queue
			virtual auto finish(trx::transaction _trx) -> void;

		private:
			std::mutex __mtx;
			std::vector< trx::transaction > __queue;
			std::map< std::string, std::tuple< unsigned short, std::vector< trx::transaction > > > __dependencies;
		};

		/**
		   Window Parallelization class
		   ------------------------------

		   So in this algorithm, the above explained window gains meaning here

		   Requirement 1: Requests from same window can be executed in parallel
		   Requirement 2: Requests for a different window must wait for other
		   window to finish. 
		**/
		class WindowParallelization : public trx::algorithm::Parallelization {
		public:
			WindowParallelization();
			virtual ~WindowParallelization();

			// Retrieves the next transaction to be processed and removes
			// it from the queue
			virtual auto pop() -> trx::transaction;
			// Inserts the given transaction into the waiting queue
			virtual auto push(trx::transaction _trx) -> void;
			// Signals that the given transaction has finished and rearranges
			// the queue
			virtual auto finish(trx::transaction _trx) -> void;

		private:
			std::mutex __mtx;
			std::vector< trx::transaction > __queue;
			std::vector< trx::transaction > __waiting;
			trx::timestamp_t __window;
			uint32_t __running;
		};

		/**
		   Record Parallelization class
		   ------------------------------

		   This one is the most complex one. 
		   Basically the way this works is: 

		   The coordinator looks at the changed line like:

		   T1 changes table2 line 1
		   T2 changes table3 line 40
		   T3 changes table1 line 2
		   T4 changes table2 line 1

		   Here means that T1, T2 and T3 can be executed in parallel, but T4 must
		   wait for this parallelization window to finish.
		**/
		class RecordParallelization : public trx::algorithm::Parallelization {
		public:
			RecordParallelization();
			virtual ~RecordParallelization();

			// Retrieves the next transaction to be processed and removes
			// it from the queue
			virtual auto pop() -> trx::transaction;
			// Inserts the given transaction into the waiting queue
			virtual auto push(trx::transaction _trx) -> void;
			// Signals that the given transaction has finished and rearranges
			// the queue
			virtual auto finish(trx::transaction _trx) -> void;

		private:
			std::mutex __mtx;
			std::vector< trx::transaction > __queue;
			std::map< std::string, std::tuple< unsigned short, std::vector< trx::transaction > > > __dependencies;
		};
	}
	
}
