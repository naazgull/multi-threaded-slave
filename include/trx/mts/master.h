/*
Copyright (c) 2016
*/

#pragma once

#include <trx/mts.h>

namespace trx {

	/**
	   Binlog class
	   ------------
	   
	   Simple representation to the binary log write operations. Will send transactions,
	   to slave processes, through socket streams.
	 **/
	class BinlogData : public std::vector< trx::socketstream_ptr > {
	public:
		BinlogData();
		virtual ~BinlogData();

		// Overrides original std::vector::push_back in order to make it
		// a thread-safe 
		virtual auto push_back(trx::socketstream_ptr&& _new) -> void;
		// Overrides original std::vector::push_back in order to make it
		// a thread-safe 
		virtual auto push_back(const trx::socketstream_ptr& _new) -> void;
		// Commits the transaction to the binary log a.k.a., send it to slaves
		virtual auto log(trx::transaction _trx) -> void;
		// Calculates and stores the next window initial timestamp
		virtual auto next_window() -> void;
		
	private:
		std::mutex __mtx;
		trx::timestamp_t __window;
	};
	/**
	   Smart pointer class to encapsulate the binlog data
	 **/
	class binlog : public std::shared_ptr< trx::BinlogData > {
	public:
		binlog();
		virtual ~binlog();
	};

	namespace callback {
		/**
		   Write Thread callback
		   ----------------------

		   Main cycle callback for master writing threads
		 **/
		auto write(trx::binlog _binlog) -> void;

		/**
		   Window ticker Thread callback
		   ----------------------

		   Main cycle callback for the master window ticker thread
		 **/
		auto tick(trx::binlog _binlog) -> void;
	}
}

