/*
Copyright (c) 2016
*/

#pragma once

#include <thread>
#include <trx/mts.h>

namespace trx {

	/**
	   Parallelization Algorithms choice
	   ---------------------------------

	   According to the defined preprocessor variable, a given algorithm is choosen. Pass the
	   wanted variable in the "configure" command:
	   
	   $ ./configure --prefix=/usr sysconf=/etc "CXXFLAGS=-O3 -Wall -DRECORD_PARALLEL"

	 **/
#if defined(TABLE_PARALLEL)
	typedef trx::parallel< trx::algorithm::TableParallelization > parallel_type;
#elif defined(WINDOW_PARALLEL)
	typedef trx::parallel< trx::algorithm::WindowParallelization > parallel_type;
#elif defined(RECORD_PARALLEL)
	typedef trx::parallel< trx::algorithm::RecordParallelization > parallel_type;
#endif

	namespace callback {
		/**
		   Worker Thread callback
		   ----------------------

		   Main cycle callback for slave worker threads
		 **/
		auto work(trx::parallel_type _queue, trx::database _db) -> void;

		/**
		   Coordinator Thread callback
		   ----------------------

		   Main cycle callback for slave coordinator thread
		 **/
		auto route(trx::parallel_type _queue, uint32_t _max_threads) -> void;
	}
}

