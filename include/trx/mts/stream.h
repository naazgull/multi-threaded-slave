/*
Copyright (c) 2016
*/

#pragma once

#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <streambuf>
#include <iostream>
#include <istream>
#include <ostream>
#include <memory>
#include <strings.h>
#include <cstring>
#include <unistd.h>
#include <errno.h>
#include <sstream>
#include <regex>

using namespace std;
#if !defined __APPLE__
using namespace __gnu_cxx;
#endif

namespace trx {

	/**
	   Utility classes for communication
	   ---------------------------------

	   Classes that abstract low level C sockets behind the C++ io stream interface. 
	   This code was not developed specially for this exercise, is was just adapted, it is code that I've
	   been using and optimizing over the years.
	   In this case, I've decided to use the Websocket framing, not the best but simple enough to 
	   implement in the current time frame. A more efficient framing would be the one used in 0mq sockets.
	 **/
			
	template<typename Char>
	class basic_socketbuf : public std::basic_streambuf<Char> {
	public:
		typedef Char __char_type;
		typedef std::basic_streambuf<__char_type> __buf_type;
		typedef std::basic_ostream<__char_type> __stream_type;
		typedef typename __buf_type::int_type __int_type;
		typedef typename std::basic_streambuf<Char>::traits_type __traits_type;

	protected:

		static const int char_size = sizeof(__char_type);
		static const int SIZE = 128;
		__char_type obuf[SIZE];
		__char_type ibuf[SIZE];

		int __sock;
		struct sockaddr_in __server;
		std::string __host;
		short __port;
		short __protocol;

	public:
		basic_socketbuf() : __sock(0), __port(-1) {
			__buf_type::setp(obuf, obuf + (SIZE - 1));
			__buf_type::setg(ibuf, ibuf, ibuf);
		};

		virtual ~basic_socketbuf() {
			sync();
		}

		void set_socket(int _sock) {
			this->__sock = _sock;
			if (_sock != 0) {
				int iOption = 1; 
				setsockopt(this->__sock, SOL_SOCKET, SO_KEEPALIVE, (const char *) &iOption,  sizeof(int));
				struct linger a;
				a.l_onoff = 1;
				a.l_linger = 30;
				setsockopt(this->__sock, SOL_SOCKET, SO_LINGER, (char*) &a, sizeof(a));
			}
		}

		int get_socket() {
			return this->__sock;
		}

		struct sockaddr_in& server() {
			return this->__server;
		}

		std::string& host() {
			return this->__host;
		}

		short& port() {
			return this->__port;
		}

		virtual bool __good() {
			return this->__sock != 0;
		}

	protected:

		int output_buffer() {
			if (!__good()) {
				return __traits_type::eof();
			}
			
			int _num = __buf_type::pptr() - __buf_type::pbase();
			int _actually_written = -1;
			if ((_actually_written = ::send(__sock, reinterpret_cast<char*>(obuf), _num * char_size, MSG_NOSIGNAL)) < 0) {
				::shutdown(this->__sock, SHUT_RDWR);
				::close(this->__sock);
				this->__sock = 0;
				return 0;
			}
			__buf_type::pbump(-_actually_written);
			return _actually_written;
		}

		virtual __int_type overflow(__int_type c) {
			if (c != __traits_type::eof()) {
				*__buf_type::pptr() = c;
				__buf_type::pbump(1);
			}

			if (output_buffer() == __traits_type::eof()) {
				return __traits_type::eof();
			}
			return c;
		}

		virtual int sync() {
			if (output_buffer() == __traits_type::eof()) {
				return __traits_type::eof();
			}
			return 0;
		}

		virtual __int_type underflow() {
			if (__buf_type::gptr() < __buf_type::egptr()) {
				return *__buf_type::gptr();
			}

			if (!__good()) {
				return __traits_type::eof();
			}

			int _actually_read = -1;
			if ((_actually_read = ::recv(__sock, reinterpret_cast<char*>(ibuf), SIZE * char_size, 0)) < 0) {
				::shutdown(this->__sock, SHUT_RDWR);
				::close(this->__sock);
				this->__sock = 0;
				return __traits_type::eof();
			}
			if (_actually_read == 0) {
				return __traits_type::eof();
			}
			__buf_type::setg(ibuf, ibuf, ibuf + _actually_read);
			return *__buf_type::gptr();
		}
	};

	typedef basic_socketbuf<char> socketbuf;
	typedef basic_socketbuf<wchar_t> wsocketbuf;

	template<typename Char>
	class basic_socketstream : public std::basic_iostream<Char> {
	public:
		typedef Char __char_type;
		typedef std::basic_iostream<__char_type> __stream_type;
		typedef basic_socketbuf<__char_type> __buf_type;

	protected:
		__buf_type __buf;
		bool __is_error;

	public:
		basic_socketstream() :
			__stream_type(&__buf), __is_error(false) {
		};

		basic_socketstream(int s) : __stream_type(&__buf), __is_error(false) {
			__buf.set_socket(s);
		}
		virtual ~basic_socketstream() {
			__stream_type::flush();
			__stream_type::clear();
			__buf.set_socket(0);
		}

		void assign(int _sockfd) {
			__buf.set_socket(_sockfd);
		}

		void unassign() {
			__buf.set_socket(0);
		}

		void close() {
			__stream_type::flush();
			__stream_type::clear();
			if (__buf.get_socket() != 0) {
				::shutdown(__buf.get_socket(), SHUT_RDWR);
				::close(__buf.get_socket());
			}
			__buf.set_socket(0);
		}

		bool is_open() {
			return (!__is_error && __buf.get_socket() != 0 && __buf.__good());
		}

		bool ready() {
			fd_set sockset;
			FD_ZERO(&sockset);
			FD_SET(__buf.get_socket(), &sockset);
			return select(__buf.get_socket() + 1, &sockset, nullptr, nullptr, nullptr) == 1;
		}

		__buf_type& buffer() {
			return this->__buf;
		}

		bool open(const std::string& _host, uint16_t _port) {
			this->close();
			__buf.host() = _host;
			__buf.port() = _port;
			
			::hostent *_he = gethostbyname(_host.c_str());
			if (_he == nullptr) {
				return false;
			}

			std::copy(reinterpret_cast<char*>(_he->h_addr), reinterpret_cast<char*>(_he->h_addr) + _he->h_length, reinterpret_cast<char*>(& __buf.server().sin_addr.s_addr));
			__buf.server().sin_family = AF_INET;
			__buf.server().sin_port = htons(_port);

			int _sd = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
			if (::connect(_sd, reinterpret_cast<sockaddr*>(& __buf.server()), sizeof(__buf.server())) < 0) {
				__stream_type::setstate(std::ios::failbit);
				__buf.set_socket(0);
				__is_error = true;
				return false;
			}
			else {
				__buf.set_socket(_sd);
			}
			return true;
		}

		bool read(std::string& _out, int* _op_code) {
			unsigned char _hdr;
			(* this) >> std::noskipws >> _hdr;

			bool _fin = _hdr & 0x80;
			*_op_code = _hdr & 0x0F;
			(* this) >> std::noskipws >> _hdr;
			//bool _mask = _hdr & 0x80;
			std::string _masked;

			int _len = _hdr & 0x7F;
			if (_len == 126) {
				(* this) >> std::noskipws >> _hdr;
				_len = (int) _hdr;
				_len <<= 8;
				(* this) >> std::noskipws >> _hdr;
				_len += (int) _hdr;
			}
			else if (_len == 127) {
				(* this) >> std::noskipws >> _hdr;
				_len = (int) _hdr;
				for (int _i = 0; _i < 7; _i++) {
					_len <<= 8;
					(* this) >> std::noskipws >> _hdr;
					_len += (int) _hdr;
				}
			}

			for (int _i = 0; _i != _len; _i++) {
				(* this) >> std::noskipws >> _hdr;
				_masked.push_back((char) _hdr);
			}
			_out.assign(_masked);

			return _fin;
		}

		bool write(std::string _in) {
			int _len = _in.length();

			if (!this->is_open()) {
				return false;
			}
			(* this) << (unsigned char) 0x81;
			if (_len > 65535) {
				(* this) << (unsigned char) 0x7F;
				(* this) << (unsigned char) 0x00;
				(* this) << (unsigned char) 0x00;
				(* this) << (unsigned char) 0x00;
				(* this) << (unsigned char) 0x00;
				(* this) << ((unsigned char) ((_len >> 24) & 0xFF));
				(* this) << ((unsigned char) ((_len >> 16) & 0xFF));
				(* this) << ((unsigned char) ((_len >> 8) & 0xFF));
				(* this) << ((unsigned char) (_len & 0xFF));
			}
			else if (_len > 125) {
				(* this) << (unsigned char) 0x7E;
				(* this) << ((unsigned char) (_len >> 8));
				(* this) << ((unsigned char) (_len & 0xFF));
			}
			else {
				(* this) << (unsigned char) (0x80 | ((unsigned char) _len));
			}

			(* this) << _in << std::flush;
			return true;
		}

	};

	typedef basic_socketstream<char> socketstream;
	typedef std::shared_ptr< trx::socketstream > socketstream_ptr;

	template<typename Char>
	class basic_serversocketstream {
	public:
		typedef Char __char_type;
		typedef std::basic_iostream<__char_type> __stream_type;
		typedef basic_socketbuf<__char_type> __buf_type;

	protected:
		int __sockfd;

	public:
		basic_serversocketstream() : __sockfd(0) {
		}

		virtual ~basic_serversocketstream() {
			this->close();
		}

		void close() {
			::shutdown(__sockfd, SHUT_RDWR);
			::close(__sockfd);
		}

		bool is_open() {
			return __sockfd != 0;
		}

		bool ready() {
			fd_set sockset;
			FD_ZERO(&sockset);
			FD_SET(__sockfd, &sockset);
			return select(__sockfd + 1, &sockset, nullptr, nullptr, nullptr) == 1;
		}

		__buf_type& buffer() {
			return this->__buf;
		}

		bool bind(uint16_t _port) {
			this->__sockfd = socket(AF_INET, SOCK_STREAM, 0);
			if (this->__sockfd < 0) {
				return false;
			}

			int _opt = 1;
			if (setsockopt(this->__sockfd, SOL_SOCKET, SO_REUSEADDR, (char *) &_opt, sizeof(_opt)) == SO_ERROR) {
				::shutdown(this->__sockfd, SHUT_RDWR);
				::close(this->__sockfd);
				this->__sockfd = 0;
				return false;
			}

			struct sockaddr_in _serv_addr;
			bzero((char *) &_serv_addr, sizeof(_serv_addr));
			_serv_addr.sin_family = AF_INET;
			_serv_addr.sin_addr.s_addr = INADDR_ANY;
			_serv_addr.sin_port = htons(_port);
			if (::bind(this->__sockfd, (struct sockaddr *) &_serv_addr, sizeof(_serv_addr)) < 0) {
				::shutdown(this->__sockfd, SHUT_RDWR);
				::close(this->__sockfd);
				this->__sockfd = 0;
				return false;
			}
			::listen(this->__sockfd, 100);
			return true;
		}

		bool accept(socketstream* _out) {
			if (this->__sockfd != -1) {
				struct sockaddr_in* _cli_addr = new struct sockaddr_in();
				socklen_t _clilen = sizeof(struct sockaddr_in);
				int _newsockfd = ::accept(this->__sockfd, (struct sockaddr *) _cli_addr, &_clilen);

				if (_newsockfd < 0) {
					return false;
				}

				struct linger _so_linger;
				_so_linger.l_onoff = 1;
				_so_linger.l_linger = 30;
				::setsockopt(_newsockfd,SOL_SOCKET, SO_LINGER, &_so_linger, sizeof _so_linger);
				_out->assign(_newsockfd);
				return true;
			}
			return false;
		}

		bool accept(int* _out) {
			if (this->__sockfd != -1) {
				struct sockaddr_in* _cli_addr = new struct sockaddr_in();
				socklen_t _clilen = sizeof(struct sockaddr_in);
				int _newsockfd = ::accept(this->__sockfd, (struct sockaddr *) _cli_addr, &_clilen);

				if (_newsockfd < 0) {
					return false;
				}

				struct linger _so_linger;
				_so_linger.l_onoff = 1;
				_so_linger.l_linger = 30;
				::setsockopt(_newsockfd,SOL_SOCKET, SO_LINGER, &_so_linger, sizeof _so_linger);
				*_out = _newsockfd;
				return true;
			}
			return false;
		}
	};

	typedef basic_serversocketstream<char> serversocketstream;
	typedef std::shared_ptr< trx::serversocketstream > serversocketstream_ptr;

	/**
	   Base64 utility methods
	   ----------------------

	   Utility methods to marshal and unmarshal Base64 based encoded strings
	 **/
	namespace base64 {
	
		const char encodeCharacterTable[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		const char decodeCharacterTable[256] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

		auto r_encode(std::string _in) -> std::string;
		auto encode(std::string& _out) -> void;
		auto r_decode(std::string _in) -> std::string;
		auto decode(std::string& _out) -> void;
		
	}	

	/**
	   String to number convertion
	   ---------------------------

	   String to uint32_t and to timestamp_t.
	 **/
	auto uint_from_str(std::string _s) -> uint32_t;
	auto ts_from_str(std::string _s) -> trx::timestamp_t;

	/**
	   UUID Generator
	   --------------

	   Generator from OSSP for Universal Unique Identifiers.
	 **/
	extern uuid uuid_gen;
	auto uuid() -> std::string;
	
}
