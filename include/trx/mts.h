/*
Copyright (c) 2016
*/

#pragma once

#include <map>
#include <memory>
#include <ossp/uuid++.hh>
#include <mutex>

using namespace std;
#if !defined __APPLE__
using namespace __gnu_cxx;
#endif

namespace trx {

	typedef unsigned long long timestamp_t;
	
	/**
	   Transaction information classes
	   ----------------------------
	   
	   Hold the information about a transaction and utility methods to serialize and deserialize instance objects
	 **/
	class transaction;
	class TransactionData;

	class TransactionData {
	public:
		TransactionData(std::string _table, uint32_t _key, std::string _data);
		virtual ~TransactionData();

		// Global Transaction ID getter
		virtual auto gtid() -> std::string;
		// Global Transation ID setter
		virtual auto gtid(std::string  _table) -> void;
		// Table name getter		
		virtual auto table() -> std::string;
		// Table name setter
		virtual auto table(std::string  _table) -> void;
		// Record key getter
		virtual auto key() -> uint32_t;
		// Record key setter
		virtual auto key(uint32_t  _key) -> void;
		// Record data getter
		virtual auto data() -> std::string;
		// Record data setter
		virtual auto data(std::string  _data) -> void;
		// Timestamp getter
		virtual auto timestamp() -> trx::timestamp_t;
		// Timestamp setter
		virtual auto timestamp(trx::timestamp_t _ts) -> void;
		// Transform the transaction data into a string
		virtual auto serialize() -> std::string;
		
	private:
		std::string __gtid;
		std::string __table;
		uint32_t __key;
		std::string __data;
		trx::timestamp_t __timestamp;
	};
	/**
	   Smart pointer class to encapsulate the transaction data
	 **/
	class transaction : public std::shared_ptr< trx::TransactionData > {
	public:
		transaction();
		transaction(std::string _table, uint32_t _key, std::string _data);
		virtual ~transaction();

		// Transform the string representation of a string into a the transaction data
		static auto deserialize(std::string _data) -> trx::transaction;
		
	};

	/**
	   Table class
	   -----------

	   Simple representation of a TABLE
	 **/
	class TableData : public std::map< uint32_t, std::string > {
	public:
		TableData(std::string _name);
		virtual ~TableData();

		// Table class getter
		virtual auto name() -> std::string;
		// Stores the trx.data with trx.key as identifier
		virtual auto store(trx::transaction _trx) -> void;

	private:
		std::string __name;
		std::mutex __mtx;
	};
	/**
	   Smart pointer class to encapsulate the table data
	 **/
	class table : public std::shared_ptr< trx::TableData > {
	public:
		table(std::string _name);
		virtual ~table();
	};

	/**
	   Database class
	   --------------

	   Simple representation of a DATABASE
	 **/
	class DatabaseData : public std::map< std::string, trx::table > {
	public:
		DatabaseData(std::string _name);
		virtual ~DatabaseData();

		// Database class getter
		virtual auto name() -> std::string;
		// Stores the trx.data with trx.key as identifier in trx.table
		virtual auto store(trx::transaction _trx) -> void;

	private:
		std::string __name;
	};
	/**
	   Smart pointer class to encapsulate the database data
	 **/
	class database : public std::shared_ptr< trx::DatabaseData > {
	public:
		database(std::string _name);
		virtual ~database();
		
	};

}

#include <trx/mts/stream.h>
#include <trx/mts/algorithms.h>
#include <trx/mts/slave.h>
#include <trx/mts/master.h>
#include <trx/mts/config.h>
