# Disclaimer

My formal background is not in this area and, therefore, I've researched a little 
about the problem and the way MySQL/Oracle solves it. 

Although I've read a bit about it, I tried to implement the solution not based on 
documentation and papers but in my own interpretation of the exercise statement, 
since, I think, that is the purpose.

I did not implement the bonus requirement, honestly, don't know if I could do it 
properly without adding a few more days to the overall task time.

# Installation

This project has only been tested in Ubuntu 17.10 and don't know how it work in 
some environments. Besides a library for UUID generation, the used code is standard C++ 
and should run in POSIX environments.

## Dependencies

- OSSP UUID library (http://www.ossp.org/pkg/lib/uuid/)

In Ubuntu or Debian based environment:

	$ sudo apt install libossp-uuid-dev

## Build

	$ tar xvzf multi-threaded-slave-0,0,1.tar.gz
	$ cd multi-threaded-slave
	$ autoreconf -vfi
	$ ./configure --prefix=/usr sysconf=/etc "CXXFLAGS=-O3 -Wall -DTABLE_PARALLEL"
	$ make -j4
	$ sudo make install

### Configuring for Table Parallelization algorithm

	$ ./configure --prefix=/usr sysconf=/etc "CXXFLAGS=-O3 -Wall -DTABLE_PARALLEL"

### Configuring for Window Parallelization algorithm

	$ ./configure --prefix=/usr sysconf=/etc "CXXFLAGS=-O3 -Wall -DWINDOW_PARALLEL"

### Configuring for Record Parallelization algorithm ####

	$ ./configure --prefix=/usr sysconf=/etc "CXXFLAGS=-O3 -Wall -DRECORD_PARALLEL"

# Running

In one terminal run:

	$ trx-master 1025 4
	
In another terminal run:

	$ trx-slave 4
	
# File Structure

The relevant files are:

	├── include/
	│   └── trx/
	│       ├── mts/
	│       │   ├── algorithms.h
	│       │   ├── master.h
	│       │   ├── slave.h
	│       │   └── stream.h
	│       └── mts.h
	└── src/
	    ├── master/
	    │   └── main.cpp
	    ├── mts/
	    │   ├── record.cpp
	    │   ├── table.cpp
	    │   └── window.cpp
	    ├── slave/
	    │   └── main.cpp
	    └── stream/
	        ├── data.cpp
	        ├── socket.cpp
	        └── trx.cpp


- **include/trx/mts.h**: declaration for the nuclear classes for transaction, tables and 
databases
- **include/trx/mts/algorithms.h**: declaration for the parallelization algorithm classes
- **include/trx/mts/master.h**: declaration of functions and callbacks for the master process
- **include/trx/mts/slave.h**: declaration of functions and callbacks for the slave processes
- **include/trx/mts/stream.h**: declaration of classes used in socket communication
- **src/master/main.cpp**: implementation of the master process, binary log and writer threads 
life-cycle
- **src/mts/record.cpp**: implementaiton of the changed line parallelization algorithm
- **src/mts/table.cpp**: implementaiton of the table parallelization algorithm
- **src/mts/window.cpp**: implementaiton of the window parallelization algorithm
- **src/slave/main.cpp**: implementation of the slave process and slave worker threads 
life-cycle
- **src/stream/data.cpp**: implementation of the table and database classes
- **src/stream/socket.cpp**: implementation of the soket communication classes
- **src/stream/trx.cpp**: implementation of the transaction class

# Classes

## trx::transaction

_(in include/trx/mts.h, src/stream/trx.cpp)_

The class that holds the information for a transaction. It has the following 
instance attributes:

- _table_: the name for the table the transaction will operate upon
- _key_: the line number for the record the transaction will write
- _timestamp_: the initial timestamp, in milliseconds, for windows
- _data_: the data that will be written into the record

Each transaction is serialized into a string with the following representation:
	
	(<base64:table>,<key>,<timestamp>,<base64:data>)
	
(Base64 not the most efficient solution, I know, but a simple one that allows 
a very simple parsing process)	

## trx::parallel< C >

_(in include/trx/mts/algorithms.h)_

A template class that interfaces with the actual parallelization algorithm class, 
for instance:
	
```
typedef trx::parallel< trx::algorithm::TableParallelization > parallel_type;
```

## trx::algorithm::TableParallelization

_(in include/trx/mts/algorithms.h, src/mts/table.cpp)_

The class that implements the Table parallelization algorithm as described in the 
exercise statement. 

I used a vector to keep que commitable transactions queue and an hash map to keep 
a list of pending transaction, per table. Whenever a transaction is finished, the 
queue for the given table is processed and if there is any transaction available, 
it will be moved to the commitable transactions queue.

## trx::algorithm::WindowParallelization

_(in include/trx/mts/algorithms.h, src/mts/window.cpp)_

The class that implements the Window parallelization algorithm as described in the 
exercise statement. 

I used two vectors, one to keep que commitable transactions queue and the other to 
keep the waiting transactions. Whenever a transaction is finished, a check is made 
for running transactions that have the same parent window. Whenever all transaction 
with the same parent window are finished, the algorithm moves on to the next window, 
moving all suitable transactions to the commitable transactions queue.

## trx::algorithm::RecordParallelization

_(in include/trx/mts/algorithms, src/mts/record.cpp)_

The class that implements the Changed Line parallelization algorithm as described in
the exercise statement. 

I used a vector to keep que commitable transactions queue and an hash map to keep a
list of pending transaction, per table, per record line. Whenever a transaction is
finished, the queue for the given table/line is processed and if there is any 
transaction available, it will be moved to the commitable transactions queue.

## trx::binlog

_(in include/trx/mts/master.h, src/master/main.cpp)_

Encapsulates the socket communication and the synced writing to the log.

# Master proces life-cycle

_(in src/master/main.cpp)_

The master process is composed by:

- _a main thread_, that accepts connections from the slaves and adds the new socket to 
the binlog output streams
- _a ticker thread_, that moves the temporal window for transaction steps
- _writer threads_ that simulate several clients writing to the database

Random transactions and random execution times are implemented.

# Slave process life-cycle

_(in src/master/slave.cpp)_

The slave process is componsed by:

- _a main thread_ that is polling the connections with all the master processes and 
reading transction data
- _worker threads_, that try to retrieve the next transaction to be commited.

Random execution times are implemented.

# Not implemented / Issues

- No exceptions, just error handling with messages to the standard error stream
- No socket connection recovery
- No memory leak tests performed
- No configuration file
- No algorithm optimization (pretty sure it will break with a lot of activity)
- No message compression
- Each slave can connect and receive messages from more than one master, but didn't 
explore much, this feature
