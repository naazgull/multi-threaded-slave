#include <trx/mts.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <errno.h>
#include <cassert>
#include <signal.h>
#include <random>

#define MASTERS { { "localhost", 1025 } }

auto terminate(int _signal) -> void {
	key_t _key = ftok("/usr/bin/trx-slave", 1);
	int _sem = semget(_key, 1, IPC_CREAT | 0777);
	semctl(_sem, 0, IPC_RMID);
	exit(_signal);
}

auto trx::callback::work(trx::parallel_type _queue, trx::database _db) -> void {
	uint16_t _max_sleep = 100; // milliseconds
	std::random_device _rd;
	std::mt19937 _gen(_rd());
	std::uniform_int_distribution<> _ms_dis(0, _max_sleep);

	while (true) {
		trx::transaction _trx = _queue->pop();
		uint32_t _ms_sleep = _ms_dis(_gen);
		usleep(_ms_sleep * 1000);
		std::cout << "storing (" << _trx->table() << "," << _trx->key() << "," << _trx->timestamp() << "," << _trx->data() << ")" << endl << flush;
		_db->store(_trx);
		_queue->commit(_trx);
	}
}

auto trx::callback::route(trx::parallel_type _queue, uint32_t _max_threads) -> void {
	std::vector< std::tuple< std::string, uint16_t > > _masters = MASTERS;
	int _epoll_fd = epoll_create1(0);
	uint32_t _max_events = _max_threads * 3 + 1;
	
	if (_epoll_fd == -1) {
		std::cerr << "error while creating epoll file descriptor" << endl << flush;
		return;
	}

	for (auto _master : _masters) {
		trx::socketstream* _socket_stream = new trx::socketstream();
		_socket_stream->open(std::get<0>(_master), std::get<1>(_master));
		
		struct epoll_event _event;
		_event.data.ptr = _socket_stream;
		_event.events = EPOLLIN;
		if (epoll_ctl(_epoll_fd, EPOLL_CTL_ADD, _socket_stream->buffer().get_socket(), &_event) == -1) {
			std::cerr << "could not connect to " << std::get<0>(_master) << ":" << std::get<1>(_master) << endl << flush;
		}
	}

	struct epoll_event _event, _current;
	struct epoll_event* _possible_events = static_cast<epoll_event*>(calloc(_max_events, sizeof (_event)));

	while (true) {
		int _n_available = epoll_wait(_epoll_fd, _possible_events, _max_events, -1);

		for (int _i = 0; _i < _n_available; _i++) {
			_current = _possible_events[_i];
			trx::socketstream* _socket_stream = static_cast< trx::socketstream* >(_current.data.ptr);

			if ((_current.events & EPOLLERR) || (_current.events & EPOLLHUP) || (!(_current.events & EPOLLIN))) {
				std::cerr << "error while reading from file descriptor" << endl << flush;
				epoll_ctl(_epoll_fd, EPOLL_CTL_DEL, _socket_stream->buffer().get_socket(), nullptr);
				_socket_stream->close();
				delete _socket_stream;
			}
			else if (_current.events & EPOLLRDHUP) {
				std::cerr << "peer closed the connection... closing on our side" << endl << flush;
				epoll_ctl(_epoll_fd, EPOLL_CTL_DEL, _socket_stream->buffer().get_socket(), nullptr);
				_socket_stream->close();
				delete _socket_stream;
			}
			else {
				std::string _message;
				int _op_code = 0;
				_socket_stream->read(_message, &_op_code);
				try {
					trx::transaction _trx = trx::transaction::deserialize(_message);
					_queue->push(_trx);
				}
				catch (std::exception& _e) {
					std::cerr << "peer closed the connection... closing on our side" << endl << flush;
					epoll_ctl(_epoll_fd, EPOLL_CTL_DEL, _socket_stream->buffer().get_socket(), nullptr);
					_socket_stream->close();
					delete _socket_stream;
				}

			}
		}
	}

	free(_possible_events);
}

auto main(int _argc, char* _argv[]) -> int {
	uint32_t _max_threads = 4;
	if (_argc >= 2) {
		_max_threads = trx::uint_from_str(_argv[1]);
	}

	::signal(SIGINT, terminate);
	::signal(SIGTERM, terminate);
	::signal(SIGABRT, terminate);
	::signal(SIGSEGV, terminate);

	trx::parallel_type _queue;
	trx::database _db("db1");
	
	for (unsigned short _nt = 0; _nt != _max_threads; _nt++) {
		std::thread _worker(trx::callback::work, _queue, _db);
		_worker.detach();
	}

	trx::callback::route(_queue, _max_threads);
	return 0;
}
