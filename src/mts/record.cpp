#include <trx/mts.h>

trx::algorithm::RecordParallelization::RecordParallelization() {
}

trx::algorithm::RecordParallelization::~RecordParallelization() {
}

auto trx::algorithm::RecordParallelization::pop() -> trx::transaction {
	trx::transaction _result;
	{ std::lock_guard< std::mutex > _l(this->__mtx);
		_result = this->__queue.front();
		this->__queue.erase(this->__queue.begin()); }
	return _result;
}

auto trx::algorithm::RecordParallelization::push(trx::transaction _trx) -> void {
	{ std::lock_guard< std::mutex > _l(this->__mtx);
		std::string _key = _trx->table() + std::string("/") + std::to_string(_trx->key());
		auto _found = this->__dependencies.find(_key);
		bool _go_ahead = _found == this->__dependencies.end() || std::get<0>(_found->second) == 0;

		if (_found == this->__dependencies.end()) {
			std::vector< trx::transaction > _queue;
			this->__dependencies.insert(std::make_pair(_key, std::make_tuple(0, _queue)));
			_found = this->__dependencies.find(_key);
		}
		else if (!_go_ahead) {
			std::get<1>(_found->second).push_back(_trx);
		}
		
		if (_go_ahead) {
			this->__queue.push_back(_trx);
			std::get<0>(_found->second) = std::get<0>(_found->second) + 1;
			this->notify();
		} }
}

auto trx::algorithm::RecordParallelization::finish(trx::transaction _trx) -> void {
	{ std::lock_guard< std::mutex > _l(this->__mtx);
		std::string _key = _trx->table() + std::string("/") + std::to_string(_trx->key());
		auto _found = this->__dependencies.find(_key);
		if (_found != this->__dependencies.end()) {
			std::get<0>(_found->second) = std::get<0>(_found->second) - 1;
			if (std::get<0>(_found->second) == 0) {
				if (std::get<1>(_found->second).size() != 0) {
					this->__queue.push_back(std::get<1>(_found->second).front());
					std::get<0>(_found->second) = std::get<0>(_found->second) + 1;
					std::get<1>(_found->second).erase(std::get<1>(_found->second).begin());
					this->notify();
				}
				else {
					this->__dependencies.erase(_found);
				}
			}
		} }
}
