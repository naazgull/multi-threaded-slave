#include <trx/mts.h>

trx::algorithm::WindowParallelization::WindowParallelization() : __window(0), __running(0) {
}

trx::algorithm::WindowParallelization::~WindowParallelization() {
}

auto trx::algorithm::WindowParallelization::pop() -> trx::transaction {
	trx::transaction _result;
	{ std::lock_guard< std::mutex > _l(this->__mtx);
		_result = this->__queue.front();
		this->__queue.erase(this->__queue.begin()); }
	return _result;
}

auto trx::algorithm::WindowParallelization::push(trx::transaction _trx) -> void {
	{ std::lock_guard< std::mutex > _l(this->__mtx);
		if (this->__window == 0) {
			this->__window = _trx->timestamp();
		}
		if (_trx->timestamp() == this->__window) {
			this->__queue.push_back(_trx);
			this->__running++;
			this->notify();
		}
		else {
			this->__waiting.push_back(_trx);
		} }
}

auto trx::algorithm::WindowParallelization::finish(trx::transaction _trx) -> void {
	{ std::lock_guard< std::mutex > _l(this->__mtx);
		this->__running--;
		if (this->__running == 0 && this->__waiting.size() != 0) {
			this->__window = this->__waiting.front()->timestamp();
			while (this->__waiting.front()->timestamp() == this->__window) {
				this->__queue.push_back(this->__waiting.front());
				this->__waiting.erase(this->__waiting.begin());
				this->__running++;
				this->notify();
			}
		} }
}
