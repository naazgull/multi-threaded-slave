#include <trx/mts.h>
#include <cmath>
#include <random>

trx::binlog::binlog() : std::shared_ptr< trx::BinlogData >(new trx::BinlogData()) {
}

trx::binlog::~binlog() {
}

trx::BinlogData::BinlogData() {
}

trx::BinlogData::~BinlogData() {
}

auto trx::BinlogData::push_back(trx::socketstream_ptr&& _new) -> void {
	std::lock_guard< std::mutex > _l(this->__mtx);
	std::vector< trx::socketstream_ptr >::push_back(_new);
}

auto trx::BinlogData::push_back(const trx::socketstream_ptr& _new) -> void {
	std::lock_guard< std::mutex > _l(this->__mtx);
	std::vector< trx::socketstream_ptr >::push_back(_new);
}

auto trx::BinlogData::log(trx::transaction _trx) ->  void {
	std::lock_guard< std::mutex > _l(this->__mtx);
	_trx->timestamp(this->__window);
	for (auto _slave_socket : (*this)) {
		_slave_socket->write(_trx->serialize());
	}
}

auto trx::BinlogData::next_window() ->  void {
	std::lock_guard< std::mutex > _l(this->__mtx);
	this->__window = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

auto trx::callback::write(trx::binlog _binlog) -> void {
	std::vector< std::string > _tables = { "customer", "vendor", "product", "order", "invoice", "delivery" };
	uint16_t _max_records = 20;
	uint16_t _max_sleep = 100; // milliseconds
	std::random_device _rd;
	std::mt19937 _gen(_rd());
	std::uniform_int_distribution<> _table_dis(0, 5);
	std::uniform_int_distribution<> _record_dis(0, _max_records);
	std::uniform_int_distribution<> _ms_dis(0, _max_sleep);
	
	while (true) {
		try {
			int _table_idx = _table_dis(_gen);
			int _record_idx = _record_dis(_gen);
			uint32_t _ms_sleep = _ms_dis(_gen);

			usleep(_ms_sleep * 1000);
			trx::transaction _trx(_tables.at(_table_idx), _record_idx, trx::uuid());
			_binlog->log(_trx);
			std::cout << "sent (" << _trx->table() << "," << _trx->key() << "," << _trx->timestamp() << "," << _trx->data() << ")" << endl << flush;
		}
		catch (std::out_of_range& _e) {}
	}
}

auto trx::callback::tick(trx::binlog _binlog) -> void {	
	uint16_t _window_size = 10; // milliseconds
	
	while (true) {
		usleep(_window_size * 1000);		
		_binlog->next_window();
	}
}

auto main(int _argc, char* _argv[]) -> int {
	if (_argc < 3) {
		std::cerr << "usage: trx-master <listening port> <max threads>" << endl << flush;
		return -1;
	}
	uint32_t _port = trx::uint_from_str(_argv[1]);
	uint32_t _max_threads = trx::uint_from_str(_argv[2]);
	
	trx::binlog _binlog;
	for (unsigned short _nt = 0; _nt != _max_threads; _nt++) {
		std::thread _worker(trx::callback::write, _binlog);
		_worker.detach();
	}

	std::thread _ticker(trx::callback::tick, _binlog);
	_ticker.detach();
	
	trx::serversocketstream _ssocket;
	_ssocket.bind(_port);
	std::cout << "listening on " << _port << endl << flush;
	while (true) {
		int _new_sockfd = -1;
		_ssocket.accept(&_new_sockfd);
		std::cout << "new connection received" << endl << flush;

		_binlog->push_back(trx::socketstream_ptr(new trx::socketstream(_new_sockfd)));
	}

	return 0;
}
