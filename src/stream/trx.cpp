#include <trx/mts.h>
#include <chrono>

namespace trx {
	::uuid uuid_gen;
}

trx::TransactionData::TransactionData(std::string _table, uint32_t _key, std::string _data) : __gtid(trx::uuid()), __table(_table), __key(_key), __data(_data), __timestamp(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
}

trx::TransactionData::~TransactionData() {
}

// Global Transaction ID getter
auto trx::TransactionData::gtid() -> std::string {
	return this->__gtid;
}

// Global Transaction ID setter
auto trx::TransactionData::gtid(std::string  _gtid) -> void {
	this->__gtid.assign(_gtid);
}

// Table name getter		
auto trx::TransactionData::table() -> std::string {
	return this->__table;
}

// Table name setter
auto trx::TransactionData::table(std::string  _table) -> void {
	this->__table.assign(_table);
}

// Record key getter
auto trx::TransactionData::key() -> uint32_t {
	return this->__key;
}

// Record key setter
auto trx::TransactionData::key(uint32_t _key) -> void {
	this->__key = _key;
}

// Record data getter
auto trx::TransactionData::data() -> std::string {
	return this->__data;
}

// Record data setter
auto trx::TransactionData::data(std::string  _data) -> void {
	this->__data.assign(_data);
}

// Timestamp getter
auto trx::TransactionData::timestamp() -> trx::timestamp_t {
	return this->__timestamp;
}

// Timestamp setter
auto trx::TransactionData::timestamp(trx::timestamp_t  _timestamp) -> void {
	this->__timestamp = _timestamp;
}

// Transform the transaction data into a string
auto trx::TransactionData::serialize() -> std::string {
	return std::string("(") + trx::base64::r_encode(this->__gtid) + std::string(",") + trx::base64::r_encode(this->__table) + std::string(",") + std::to_string(this->__key) + std::string(",") + std::to_string(this->__timestamp) + std::string(",") + trx::base64::r_encode(this->__data) + std::string(")");
}

trx::transaction::transaction() : std::shared_ptr< trx::TransactionData >(new trx::TransactionData("", UINT32_MAX, "")) {
}

trx::transaction::transaction(std::string _table, uint32_t _key, std::string _data) : std::shared_ptr< trx::TransactionData >(new trx::TransactionData(_table, _key, _data)) {
}

trx::transaction::~transaction() {
}

auto trx::transaction::deserialize(std::string _data) -> trx::transaction {
	static const std::regex _data_rgx("\\("
		"([^,]+),"
		"([^,]+),"
		"([^,]+),"
		"([^,]+),"
		"([^)]+)"
		"\\)"
	);
	
	std::smatch _data_matches;
	std::regex_match(_data, _data_matches, _data_rgx);

	if (((std::string) _data_matches[1]).length() == 0) {
		throw std::exception();
	}
	
	trx::transaction _result;
	_result->gtid(trx::base64::r_decode((std::string) _data_matches[1]));
	_result->table(trx::base64::r_decode((std::string) _data_matches[2]));
	_result->key(trx::uint_from_str(_data_matches[3]));
	_result->timestamp(trx::ts_from_str(_data_matches[4]));
	_result->data(trx::base64::r_decode((std::string) _data_matches[5]));
	return _result;
}

auto trx::base64::r_encode(std::string _in) -> std::string {
	std::string _out(_in.data());
	trx::base64::encode(_out);
	return _out;
}

auto trx::base64::encode(std::string& _out) -> void {
	std::istringstream in;
	char buff1[3];
	char buff2[4];
	uint8_t i = 0, j;

	in.str(_out);
	_out.assign("");

	while (in.readsome(&buff1[i++], 1))
		if (i == 3) {
			_out.push_back(encodeCharacterTable[(buff1[0] & 0xfc) >> 2]);
			_out.push_back(encodeCharacterTable[((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4)]);
			_out.push_back(encodeCharacterTable[((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6)]);
			_out.push_back(encodeCharacterTable[buff1[2] & 0x3f]);
			i = 0;
		}

	if (--i) {
		for (j = i; j < 3; j++)
			buff1[j] = '\0';

		buff2[0] = (buff1[0] & 0xfc) >> 2;
		buff2[1] = ((buff1[0] & 0x03) << 4) + ((buff1[1] & 0xf0) >> 4);
		buff2[2] = ((buff1[1] & 0x0f) << 2) + ((buff1[2] & 0xc0) >> 6);
		buff2[3] = buff1[2] & 0x3f;

		for (j = 0; j < (i + 1); j++)
			_out.push_back(encodeCharacterTable[(uint8_t) buff2[j]]);

		while (i++ < 3)
			_out.push_back('=');
	}
}

auto trx::base64::r_decode(std::string _in) -> std::string {
	std::string _out(_in.data());
	trx::base64::decode(_out);
	return _out;
}

auto trx::base64::decode(std::string& _out) -> void {
	std::istringstream in;
	std::ostringstream out;
	char buff1[4];
	char buff2[4];
	uint8_t i = 0, j;

	in.str(_out);
	_out.assign("");

	while (in.readsome(&buff2[i], 1) && buff2[i] != '=') {
		if (++i == 4) {
			for (i = 0; i != 4; i++)
				buff2[i] = decodeCharacterTable[(uint8_t) buff2[i]];

			_out.push_back((char) ((buff2[0] << 2) + ((buff2[1] & 0x30) >> 4)));
			_out.push_back((char) (((buff2[1] & 0xf) << 4) + ((buff2[2] & 0x3c) >> 2)));
			_out.push_back((char) (((buff2[2] & 0x3) << 6) + buff2[3]));

			i = 0;
		}
	}

	if (i) {
		for (j = i; j < 4; j++)
			buff2[j] = '\0';
		for (j = 0; j < 4; j++)
			buff2[j] = decodeCharacterTable[(uint8_t) buff2[j]];

		buff1[0] = (buff2[0] << 2) + ((buff2[1] & 0x30) >> 4);
		buff1[1] = ((buff2[1] & 0xf) << 4) + ((buff2[2] & 0x3c) >> 2);
		buff1[2] = ((buff2[2] & 0x3) << 6) + buff2[3];

		for (j = 0; j < (i - 1); j++)
			_out.push_back((char) buff1[j]);
	}
}

auto trx::uint_from_str(std::string _s) -> uint32_t {
	uint32_t _result;
	std::stringstream _ss;
	_ss << _s << flush;
	_ss >> _result;
	return _result;
}

auto trx::ts_from_str(std::string _s) -> trx::timestamp_t {
	trx::timestamp_t _result;
	std::stringstream _ss;
	_ss << _s << flush;
	_ss >> _result;
	return _result;
}

auto trx::uuid() -> std::string {
	trx::uuid_gen.make(UUID_MAKE_V1);
	return trx::uuid_gen.string();
}
