#include <trx/mts.h>

trx::TableData::TableData(std::string _name) : __name(_name) {
}

trx::TableData::~TableData() {
}

auto trx::TableData::name() -> std::string {
	return this->__name;
}

auto trx::TableData::store(trx::transaction _trx) -> void {
	{ std::lock_guard< std::mutex > _l(this->__mtx);
		this->insert(std::make_pair(_trx->key(), _trx->data())); }
}

trx::table::table(std::string _name) : std::shared_ptr< trx::TableData >(new trx::TableData(_name)) {
}

trx::table::~table() {
}

trx::DatabaseData::DatabaseData(std::string _name) : __name(_name) {
}

trx::DatabaseData::~DatabaseData() {
}

auto trx::DatabaseData::name() -> std::string {
	return this->__name;
}

auto trx::DatabaseData::store(trx::transaction _trx) -> void {
	auto _table = this->find(_trx->table());
	if (_table == this->end()) {
		this->insert(std::make_pair(_trx->table(), trx::table(_trx->table())));
		_table = this->find(_trx->table());
	}
	_table->second->store(_trx);
}

trx::database::database(std::string _name) : std::shared_ptr< trx::DatabaseData >(new trx::DatabaseData(_name)) {
}

trx::database::~database() {
}

